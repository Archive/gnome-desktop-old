/*  -*- Mode: C; c-set-style: linux; indent-tabs-mode: nil; c-basic-offset: 8 -*-
 * gnome-desktop: desktop manager for GNOME desktop
 * 
 * Copyright (C) 1999 Red Hat Inc., Free Software Foundation
 * (based on Midnight Commander code by Federico Mena Quintero and Miguel de Icaza)
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "gnome-desktop.h"
#include <gnome.h>
#include "desktopicon.h"
#include "item.h"
#include <gdk/gdkx.h>
#include <gtk/gtkinvisible.h>

/* Standard DnD types */
typedef enum {
	TARGET_MC_DESKTOP_ICON,
	TARGET_URI_LIST,
	TARGET_TEXT_PLAIN,
	TARGET_URL,
	TARGET_NTARGETS
} TargetType;

/* DnD target names */
#define TARGET_MC_DESKTOP_ICON_TYPE	"application/x-mc-desktop-icon"
#define TARGET_URI_LIST_TYPE 		"text/uri-list"
#define TARGET_TEXT_PLAIN_TYPE 		"text/plain"
#define TARGET_URL_TYPE			"_NETSCAPE_URL"

/* Drag and drop sources and targets */

static GtkTargetEntry dnd_icon_sources[] = {
	{ TARGET_MC_DESKTOP_ICON_TYPE, 0, TARGET_MC_DESKTOP_ICON },
	{ TARGET_URI_LIST_TYPE, 0, TARGET_URI_LIST },
	{ TARGET_TEXT_PLAIN_TYPE, 0, TARGET_TEXT_PLAIN },
	{ TARGET_URL_TYPE, 0, TARGET_URL }
};

static GtkTargetEntry dnd_icon_targets[] = {
	{ TARGET_MC_DESKTOP_ICON_TYPE, 0, TARGET_MC_DESKTOP_ICON },
	{ TARGET_URI_LIST_TYPE, 0, TARGET_URI_LIST },
	{ TARGET_URL_TYPE, 0, TARGET_URL }
};

static GtkTargetEntry dnd_desktop_targets[] = {
	{ TARGET_MC_DESKTOP_ICON_TYPE, 0, TARGET_MC_DESKTOP_ICON },
	{ TARGET_URI_LIST_TYPE, 0, TARGET_URI_LIST },
	{ TARGET_URL_TYPE, 0, TARGET_URL }
};

static int dnd_icon_nsources = sizeof (dnd_icon_sources) / sizeof (dnd_icon_sources[0]);
static int dnd_icon_ntargets = sizeof (dnd_icon_targets) / sizeof (dnd_icon_targets[0]);
static int dnd_desktop_ntargets = sizeof (dnd_desktop_targets) / sizeof (dnd_desktop_targets[0]);


/* This structure defines the information carried by a desktop icon */
typedef struct {
	GtkWidget *dicon;		/* The desktop icon widget */
        Item *item;                     /* The item the icon represents */
	int x, y;			/* Position in the desktop */
	int slot;			/* Index of the slot the icon is in, or -1 for none */
	int selected : 1;		/* Is the icon selected? */
	int tmp_selected : 1;		/* Temp storage for original selection while rubberbanding */
} DesktopIconInfo;

struct layout_slot {
	int num_icons;		      /* Number of icons in this slot */
	GList *icons;		      /* The list of icons in this slot */
};

struct _Desktop {
        
        /* Configuration options for the desktop */

        gboolean use_shaped_icons;
        gboolean use_shaped_text;
        gboolean auto_placement;
        gboolean snap_icons;
        gboolean arr_r2l;
        gboolean arr_b2t;
        gboolean arr_rows;

        /* The computed name of the user's desktop directory */
        gchar *desktop_directory;

        /* Layout information:  number of rows/columns for the layout slots, and the
         * array of slots.  Each slot is an integer that specifies the number of icons
         * that belong to that slot.
         */
        int layout_screen_width;
        int layout_screen_height;
        int layout_cols;
        int layout_rows;
        struct layout_slot *layout_slots;

        gboolean wm_is_gnome_compliant;

        /* The last icon to be selected */
        DesktopIconInfo *last_selected_icon;

        /* Proxy window for DnD and clicks on the desktop */
        GtkWidget *proxy_invisible;

        /* Right-click popup menu */
        GtkWidget *popup_menu;
        
        /* Offsets for the DnD cursor hotspot */
        int dnd_press_x, dnd_press_y;

        /* Whether a call to select_icon() is pending because the initial click on an
         * icon needs to be resolved at button release time.  Also, we store the
         * event->state.
         */
        gboolean dnd_select_icon_pending;
        guint dnd_select_icon_pending_state;

        /* Whether the button release signal on a desktop icon should be stopped due to
         * the icon being selected by clicking on the text item.
         */
        gboolean icon_select_on_text;

        /* Proxy window for clicks on the root window */
        GdkWindow *click_proxy_gdk_window;

        /* GC for drawing the rubberband rectangle */
        GdkGC *click_gc;

        /* Starting click position and event state for rubberbanding on the desktop */
        int click_start_x;
        int click_start_y;
        int click_start_state;

        /* Current mouse position for rubberbanding on the desktop */
        int click_current_x;
        int click_current_y;

        int click_dragging;

        GHashTable *icon_hash;
        
};


static void
dummy_menu_callback(GtkWidget* mi, gpointer data)
{

        g_message("Menu item selected");
}

static void
quit_callback(GtkWidget *mi, gpointer data)
{
        g_message("Exiting");
        gtk_main_quit();
}

static GnomeUIInfo gnome_panel_new_menu [] = {
	GNOMEUIINFO_ITEM_NONE (N_("_Terminal"), N_("Launch a new terminal in the current directory"), dummy_menu_callback),
	/* If this ever changes, make sure you update create_new_menu accordingly. */
	GNOMEUIINFO_ITEM_NONE (N_("_Directory..."), N_("Creates a new directory"), dummy_menu_callback),
	GNOMEUIINFO_ITEM_NONE (N_("URL L_ink..."),  N_("Creates a new URL link"), dummy_menu_callback),
	GNOMEUIINFO_ITEM_NONE (N_("_Launcher..."), N_("Creates a new launcher"), dummy_menu_callback),
	GNOMEUIINFO_END
};

/* Menu items for arranging the desktop icons */
GnomeUIInfo desktop_arrange_icons_items[] = {
	GNOMEUIINFO_ITEM_NONE (N_("By _Name"), NULL, dummy_menu_callback),
	GNOMEUIINFO_ITEM_NONE (N_("By File _Type"), NULL, dummy_menu_callback),
	GNOMEUIINFO_ITEM_NONE (N_("By _Size"), NULL, dummy_menu_callback),
	GNOMEUIINFO_ITEM_NONE (N_("By Time Last _Accessed"), NULL, dummy_menu_callback),
	GNOMEUIINFO_ITEM_NONE (N_("By Time Last _Modified"), NULL, dummy_menu_callback),
	GNOMEUIINFO_ITEM_NONE (N_("By Time Last _Changed"), NULL, dummy_menu_callback),
	GNOMEUIINFO_END
};

/* The popup menu for the desktop */
GnomeUIInfo desktop_popup_items[] = {
	GNOMEUIINFO_MENU_NEW_SUBTREE (gnome_panel_new_menu),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_SUBTREE (N_("_Arrange Icons"), desktop_arrange_icons_items),
	GNOMEUIINFO_ITEM_NONE (N_("Create _New Window"), NULL, dummy_menu_callback),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_ITEM_NONE (N_("Rescan _Desktop Directory"), NULL, dummy_menu_callback),
	GNOMEUIINFO_ITEM_NONE (N_("Rescan De_vices"), NULL, dummy_menu_callback),
	GNOMEUIINFO_ITEM_NONE (N_("Recreate Default _Icons"), NULL, dummy_menu_callback),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_ITEM_NONE (N_("Configure _Background Image"), NULL, dummy_menu_callback),
        GNOMEUIINFO_ITEM_NONE ("Quit (debug only)", NULL, quit_callback),
	GNOMEUIINFO_END
};

static int
strip_tearoff_menu_item (GnomeUIInfo *infos)
{
	GtkWidget *shell;
	GList *child_list;
	int n;

	g_assert (infos != NULL);

	shell = infos[0].widget->parent;
	child_list = gtk_container_children (GTK_CONTAINER (shell));
	n = g_list_length (child_list);

	if (child_list && GTK_IS_TEAROFF_MENU_ITEM (child_list->data)) {
		n--;
		gtk_widget_destroy (GTK_WIDGET (child_list->data));
	}

	return n;
}

/* Executes the popup menu for the desktop */
static void
desktop_popup (Desktop *desktop, GdkEventButton *event)
{
	GtkWidget *shell;
	GtkWidget *popup;
	gchar *file, *file2;
	gint i;

	/* Create the menu and then strip the tearoff menu items, sigh... */
        if (desktop->popup_menu == NULL) {
                desktop->popup_menu = gnome_popup_menu_new (desktop_popup_items);

                strip_tearoff_menu_item (desktop_arrange_icons_items);

                /* FIXME the mc/templates file defines some menu components apparently */
        }
                
	gnome_popup_menu_do_popup (desktop->popup_menu, NULL, NULL, event, NULL);

        /* 	desktop_reload_icons (FALSE, 0, 0); */
}

/* Draws the rubberband rectangle for selecting icons on the desktop */
static void
draw_rubberband (Desktop *desktop, int x, int y)
{
	int x1, y1, x2, y2;

	if (desktop->click_start_x < x) {
		x1 = desktop->click_start_x;
		x2 = x;
	} else {
		x1 = x;
		x2 = desktop->click_start_x;
	}

	if (desktop->click_start_y < y) {
		y1 = desktop->click_start_y;
		y2 = y;
	} else {
		y1 = y;
		y2 = desktop->click_start_y;
	}

	gdk_draw_rectangle (GDK_ROOT_PARENT (), desktop->click_gc, FALSE, x1, y1, x2 - x1, y2 - y1);
}

/*
 * Stores dii->selected into dii->tmp_selected to keep the original selection
 * around while the user is rubberbanding.
 */
static void
store_temp_selection (void)
{
#if 0
	int i;
	GList *l;
	DesktopIconInfo *dii;

	for (i = 0; i < (layout_cols * layout_rows); i++)
		for (l = layout_slots[i].icons; l; l = l->next) {
			dii = l->data;

			dii->tmp_selected = dii->selected;
		}
#endif
}

/**
 * icon_is_in_area:
 * @dii: the desktop icon information
 *
 * Returns TRUE if the specified icon is at least partially inside the specified
 * area, or FALSE otherwise.
 */
static int
icon_is_in_area (DesktopIconInfo *dii, int x1, int y1, int x2, int y2)
{
#if 0
	DesktopIcon *dicon;

	dicon = DESKTOP_ICON (dii->dicon);

	x1 -= dii->x;
	y1 -= dii->y;
	x2 -= dii->x;
	y2 -= dii->y;

	if (x1 == x2 && y1 == y2)
		return FALSE;

	if (x1 < dicon->icon_x + dicon->icon_w
	    && x2 >= dicon->icon_x
	    && y1 < dicon->icon_y + dicon->icon_h
	    && y2 >= dicon->icon_y)
		return TRUE;

	if (x1 < dicon->text_x + dicon->text_w
	    && x2 >= dicon->text_x
	    && y1 < dicon->text_y + dicon->text_h
	    && y2 >= dicon->text_y)
		return TRUE;

	return FALSE;
#endif
        return FALSE;
}

/* Update the selection being rubberbanded.  It selects or unselects the icons
 * as appropriate.
 */
static void
update_drag_selection (int x, int y)
{
#if 0
	int x1, y1, x2, y2;
	int i;
	GList *l;
	DesktopIconInfo *dii;
	int additive, invert, in_area;

	if (click_start_x < x) {
		x1 = click_start_x;
		x2 = x;
	} else {
		x1 = x;
		x2 = click_start_x;
	}

	if (click_start_y < y) {
		y1 = click_start_y;
		y2 = y;
	} else {
		y1 = y;
		y2 = click_start_y;
	}

	/* Select or unselect icons as appropriate */

	additive = click_start_state & GDK_SHIFT_MASK;
	invert = click_start_state & GDK_CONTROL_MASK;

	for (i = 0; i < (layout_cols * layout_rows); i++)
		for (l = layout_slots[i].icons; l; l = l->next) {
			dii = l->data;

			in_area = icon_is_in_area (dii, x1, y1, x2, y2);

			if (in_area) {
				if (invert) {
					if (dii->selected == dii->tmp_selected) {
						desktop_icon_select (DESKTOP_ICON (dii->dicon),
								     !dii->selected);
						dii->selected = !dii->selected;
					}
				} else if (additive) {
					if (!dii->selected) {
						desktop_icon_select (DESKTOP_ICON (dii->dicon), TRUE);
						dii->selected = TRUE;
					}
				} else {
					if (!dii->selected) {
						desktop_icon_select (DESKTOP_ICON (dii->dicon), TRUE);
						dii->selected = TRUE;
					}
				}
			} else if (dii->selected != dii->tmp_selected) {
				desktop_icon_select (DESKTOP_ICON (dii->dicon), dii->tmp_selected);
				dii->selected = dii->tmp_selected;
			}
		}
#endif
}

static void
unselect_all(Desktop *desktop)
{


}

/* Handles button presses on the root window via the click_proxy_gdk_window */
static gint
click_proxy_button_press (GtkWidget *widget, GdkEventButton *event, gpointer data)
{
	GdkCursor *cursor;
        Desktop *desktop = data;
        
	/* maybe the user wants to click on the icon text */
	if (event->button == 1 && desktop->use_shaped_text) {
		int x = event->x;
		int y = event->y;
#if 0
		GList *l, *icons = get_all_icons ();
		DesktopIconInfo *clicked = NULL;
		for (l = icons; l; l = l->next) {
			DesktopIconInfo *dii = l->data;
			DesktopIcon *di = DESKTOP_ICON (dii->dicon);
			int x1 = dii->x + di->text_x;
			int y1 = dii->y + di->text_y;
			int x2 = x1 + di->text_w;
			int y2 = y1 + di->text_h;
			if (x>=x1 && y>=y1 && x<=x2 && y<=y2)
				clicked = dii;
		}
		g_list_free (icons);
		if (clicked) {
			select_icon (clicked, event->state);
			return FALSE;
		}
#endif
	}

	if (event->button == 1) {
		desktop->click_start_x = event->x;
		desktop->click_start_y = event->y;
		desktop->click_start_state = event->state;

		XGrabServer (GDK_DISPLAY ());

		cursor = gdk_cursor_new (GDK_TOP_LEFT_ARROW);
		gdk_pointer_grab (GDK_ROOT_PARENT (),
				  FALSE,
				  GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK,
				  NULL,
				  cursor,
				  event->time);
		gdk_cursor_destroy (cursor);

		/* If no modifiers are pressed, we unselect all the icons */

		if ((desktop->click_start_state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK)) == 0)
			unselect_all (desktop);

		store_temp_selection (); /* Save the original selection */

		draw_rubberband (desktop, event->x, event->y);
		desktop->click_current_x = event->x;
		desktop->click_current_y = event->y;
		desktop->click_dragging = TRUE;

		return TRUE;
	} else if (event->button == 3) {
		desktop_popup (desktop, event);
		return TRUE;
	}

	return FALSE;
}

/* Terminates rubberbanding when the button is released.  This is shared by the
 * button_release handler and the motion_notify handler.
 */
static void
perform_release (Desktop *desktop, guint32 time)
{
	draw_rubberband (desktop, desktop->click_current_x, desktop->click_current_y);
	gdk_pointer_ungrab (time);
	desktop->click_dragging = FALSE;

	update_drag_selection (desktop->click_current_x, desktop->click_current_y);

	XUngrabServer (GDK_DISPLAY ());
	gdk_flush ();
}

/* Handles button releases on the root window via the click_proxy_gdk_window */
static gint
click_proxy_button_release (GtkWidget *widget, GdkEventButton *event, gpointer data)
{
        Desktop *desktop = data;
        
	if (!desktop->click_dragging || event->button != 1)
		return FALSE;

	perform_release (desktop, event->time);

	return TRUE;
}

/* Handles motion events when dragging the icon-selection rubberband on the desktop */
static gint
click_proxy_motion (GtkWidget *widget, GdkEventMotion *event, gpointer data)
{
        Desktop *desktop = data;
        
	if (!desktop->click_dragging)
		return FALSE;

	/* There exists the following race condition.  If in the button_press
	 * handler we manage to grab the server before the window manager can
	 * proxy the button release to us, then we wil not get the release
	 * event.  So we have to check the event mask and fake a release by
	 * hand.
	 */
	if (!(event->state & GDK_BUTTON1_MASK)) {
		perform_release (desktop, event->time);
		return TRUE;
	}

	draw_rubberband (desktop, desktop->click_current_x, desktop->click_current_y);
	draw_rubberband (desktop, event->x, event->y);
	update_drag_selection (event->x, event->y);
	desktop->click_current_x = event->x;
	desktop->click_current_y = event->y;
        
	return TRUE;
}

/*
 * Filter that translates proxied events from virtual root windows into normal
 * Gdk events for the proxy_invisible widget.
 */
static GdkFilterReturn
click_proxy_filter (GdkXEvent *xevent, GdkEvent *event, gpointer data)
{
	XEvent *xev;
        Desktop *desktop = data;
        
	xev = xevent;

	switch (xev->type) {
	case ButtonPress:
	case ButtonRelease:
		/* Translate button events into events that come from the proxy
		 * window, so that we can catch them as a signal from the
		 * invisible widget.
		 */
		if (xev->type == ButtonPress)
			event->button.type = GDK_BUTTON_PRESS;
		else
			event->button.type = GDK_BUTTON_RELEASE;

		gdk_window_ref (desktop->click_proxy_gdk_window);

		event->button.window = desktop->click_proxy_gdk_window;
		event->button.send_event = xev->xbutton.send_event;
		event->button.time = xev->xbutton.time;
		event->button.x = xev->xbutton.x;
		event->button.y = xev->xbutton.y;
		event->button.state = xev->xbutton.state;
		event->button.button = xev->xbutton.button;

		return GDK_FILTER_TRANSLATE;

	case DestroyNotify:
		/* The proxy window was destroyed (i.e. the window manager
		 * died), so we have to cope with it
		 */
		if (((GdkEventAny *) event)->window == desktop->click_proxy_gdk_window) {
			gdk_window_destroy_notify (desktop->click_proxy_gdk_window);
			desktop->click_proxy_gdk_window = NULL;
		}

		return GDK_FILTER_REMOVE;

	default:
		break;
	}

	return GDK_FILTER_CONTINUE;
}

/* Looks for the proxy window to get root window clicks from the window manager */
static GdkWindow *
find_click_proxy_window (void)
{
	GdkAtom click_proxy_atom;
	Atom type;
	int format;
	unsigned long nitems, after;
	Window *proxy_data;
	Window proxy;
	guint32 old_warnings;
	GdkWindow *proxy_gdk_window;

	XGrabServer (GDK_DISPLAY ());

	click_proxy_atom = gdk_atom_intern ("_WIN_DESKTOP_BUTTON_PROXY", FALSE);
	type = None;
	proxy = None;

	old_warnings = gdk_error_warnings;

	gdk_error_code = 0;
	gdk_error_warnings = 0;

	/* Check if the proxy window exists */

	XGetWindowProperty (GDK_DISPLAY (), GDK_ROOT_WINDOW (),
			    click_proxy_atom, 0,
			    1, False, AnyPropertyType,
			    &type, &format, &nitems, &after,
			    (guchar **) &proxy_data);

	if (type != None) {
		if (format == 32 && nitems == 1)
			proxy = *proxy_data;

		XFree (proxy_data);
	}

	/* The property was set, now check if the window it points to exists and
	 * has a _WIN_DESKTOP_BUTTON_PROXY property pointing to itself.
	 */

	if (proxy) {
		XGetWindowProperty (GDK_DISPLAY (), proxy,
				    click_proxy_atom, 0,
				    1, False, AnyPropertyType,
				    &type, &format, &nitems, &after,
				    (guchar **) &proxy_data);

		if (!gdk_error_code && type != None) {
			if (format == 32 && nitems == 1)
				if (*proxy_data != proxy)
					proxy = GDK_NONE;

			XFree (proxy_data);
		} else
			proxy = GDK_NONE;
	}

	gdk_error_code = 0;
	gdk_error_warnings = old_warnings;

	XUngrabServer (GDK_DISPLAY ());
	gdk_flush ();

	if (proxy)
		proxy_gdk_window = gdk_window_foreign_new (proxy);
	else
		proxy_gdk_window = NULL;

	return proxy_gdk_window;
}

/*
 * Creates a proxy window to receive clicks from the root window and
 * sets up the necessary event filters.
 */
static void
setup_desktop_click_proxy_window (Desktop *desktop)
{
	desktop->click_proxy_gdk_window = find_click_proxy_window ();
	if (!desktop->click_proxy_gdk_window) {
		desktop->wm_is_gnome_compliant = FALSE;
		g_warning ("Root window clicks will not work as no GNOME-compliant window manager could be found!");
		return;
	}
	desktop->wm_is_gnome_compliant = TRUE;

	/* Make the proxy window send events to the invisible proxy widget */
	gdk_window_set_user_data (desktop->click_proxy_gdk_window, desktop->proxy_invisible);

	/* Add our filter to get events */
	gdk_window_add_filter (desktop->click_proxy_gdk_window, click_proxy_filter, NULL);

	/*
	 * The proxy window for clicks sends us button press events with
	 * SubstructureNotifyMask.  We need StructureNotifyMask to receive
	 * DestroyNotify events, too.
	 */

	XSelectInput (GDK_DISPLAY (), GDK_WINDOW_XWINDOW (desktop->click_proxy_gdk_window),
		      SubstructureNotifyMask | StructureNotifyMask);
}

/*
 * Handler for PropertyNotify events from the root window; it must change the
 * proxy window to a new one.
 */
static gint
click_proxy_property_notify (GtkWidget *widget, GdkEventProperty *event, gpointer data)
{
        Desktop *desktop = data;
        
	if (event->window != GDK_ROOT_PARENT ())
		return FALSE;

	if (event->atom != gdk_atom_intern ("_WIN_DESKTOP_BUTTON_PROXY", FALSE))
		return FALSE;

	/* If there already is a proxy window, destroy it */

	desktop->click_proxy_gdk_window = NULL;

	/* Get the new proxy window */

	setup_desktop_click_proxy_window (desktop);

	return TRUE;
}

#define gray50_width 2
#define gray50_height 2
static char gray50_bits[] = {
  0x02, 0x01, };

/* Sets up the window manager proxy window to receive clicks on the desktop root window */
static void
setup_desktop_clicks (Desktop *desktop)
{
	GdkColormap *cmap;
	GdkColor color;
	GdkBitmap *stipple;

        /* Create the proxy window and initialize all proxying stuff */

	desktop->proxy_invisible = gtk_invisible_new ();
	gtk_widget_show (desktop->proxy_invisible);
        
	/* Make the root window send events to the invisible proxy widget */
	gdk_window_set_user_data (GDK_ROOT_PARENT (), desktop->proxy_invisible);

	/* Add our filter to get button press/release events (they are sent by
	 * the WM * with the window set to the root).  Our filter will translate
	 * them to a GdkEvent with the proxy window as its window field.
	 */
	gdk_window_add_filter (GDK_ROOT_PARENT (), click_proxy_filter, desktop);

	/* Select for PropertyNotify events from the root window */

	XSelectInput (GDK_DISPLAY (), GDK_ROOT_WINDOW (), PropertyChangeMask);

	/* Create the proxy window for clicks on the root window */
	setup_desktop_click_proxy_window (desktop);

	/* Connect the signals */

	gtk_signal_connect (GTK_OBJECT (desktop->proxy_invisible), "button_press_event",
			    (GtkSignalFunc) click_proxy_button_press,
                            desktop);
	gtk_signal_connect (GTK_OBJECT (desktop->proxy_invisible), "button_release_event",
			    (GtkSignalFunc) click_proxy_button_release,
                            desktop);
	gtk_signal_connect (GTK_OBJECT (desktop->proxy_invisible), "motion_notify_event",
			    (GtkSignalFunc) click_proxy_motion,
                            desktop);

	gtk_signal_connect (GTK_OBJECT (desktop->proxy_invisible), "property_notify_event",
			    (GtkSignalFunc) click_proxy_property_notify,
                            desktop);

	/* Create the GC to paint the rubberband rectangle */

	desktop->click_gc = gdk_gc_new (GDK_ROOT_PARENT ());

	cmap = gdk_window_get_colormap (GDK_ROOT_PARENT ());

	gdk_color_white (cmap, &color);
	if (color.pixel == 0)
		gdk_color_black (cmap, &color);

	gdk_gc_set_foreground (desktop->click_gc, &color);
	gdk_gc_set_function (desktop->click_gc, GDK_XOR);

	gdk_gc_set_fill (desktop->click_gc, GDK_STIPPLED);

	stipple = gdk_bitmap_create_from_data (NULL, gray50_bits, gray50_width, gray50_height);
	gdk_gc_set_stipple (desktop->click_gc, stipple);
	gdk_bitmap_unref (stipple);
}

static void
create_layout_info (Desktop *desktop)
{
	desktop->layout_screen_width = gdk_screen_width ();
	desktop->layout_screen_height = gdk_screen_height ();
	desktop->layout_cols = (desktop->layout_screen_width + DESKTOP_SNAP_X - 1) / DESKTOP_SNAP_X;
	desktop->layout_rows = (desktop->layout_screen_height + DESKTOP_SNAP_Y - 1) / DESKTOP_SNAP_Y;
	desktop->layout_slots = g_new0 (struct layout_slot, desktop->layout_cols * desktop->layout_rows);
}

Desktop *
desktop_new (void)
{
        Desktop *desk;
        GtkWidget *dicon;
        GdkImlibImage * image;
        
        desk = g_new0(Desktop, 1);

        setup_desktop_clicks(desk);
        
        image = gdk_imlib_load_image("/opt/gnome/share/pixmaps/gnome-logo-icon.png");

        dicon = desktop_icon_new(image, "Hello");

        gtk_widget_show(dicon);

        return desk;
}

void
desktop_destroy (Desktop *desktop)
{
	/* Destroy the desktop icons */

        /* 	destroy_desktop_icons (); */

	/* Cleanup */

	g_free (desktop->layout_slots);

	g_free (desktop->desktop_directory);

	/* Remove click-on-desktop crap */

	gdk_window_unref (desktop->click_proxy_gdk_window);

        /* popup */

        if (desktop->popup_menu) {
                gtk_widget_destroy(desktop->popup_menu);
                desktop->popup_menu = NULL;
        }
        
        /* Remove DnD crap */

	gtk_widget_destroy (desktop->proxy_invisible);
	XDeleteProperty (GDK_DISPLAY (), GDK_ROOT_WINDOW (), gdk_atom_intern ("XdndProxy", FALSE));

        /* 	g_hash_table_destroy (desktop->icon_hash);         */

        g_free(desktop);
}
